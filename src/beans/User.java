package beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

import service.BranchService;
import service.DepartmentService;

public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String loginId;
	private String password;
	private String name;
	private int isStopped;
	private int branchId;
	private int departmentId;
	private Date createdAt;
	private Date updateAt;

	/**
	 * パラメータ取得
	 * @param parameterMap
	 * @return
	 */
	public static User build(Map<String, String[]> parameterMap) {

		User user = new User();
		user.loginId = parameterMap.get("loginId")[0];
		user.password = parameterMap.get("password")[0];
		user.name = parameterMap.get("name")[0];

		Branch branch = new BranchService().getBranch( Integer.parseInt(parameterMap.get("branches")[0]) );
		Department department = new DepartmentService().getDepartment(  Integer.parseInt(parameterMap.get("departments")[0]) );
		user.branchId = branch.getId();
		user.departmentId = department.getId();

		return user;
	}

	/**
	 * idを取得します。
	 * @return id
	 */
	public int getId() {
	    return id;
	}
	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
	    this.id = id;
	}
	/**
	 * loginIdを取得します。
	 * @return loginId
	 */
	public String getLoginId() {
	    return loginId;
	}
	/**
	 * loginIdを設定します。
	 * @param loginId loginId
	 */
	public void setLoginId(String loginId) {
	    this.loginId = loginId;
	}
	/**
	 * passwordを取得します。
	 * @return password
	 */
	public String getPassword() {
	    return password;
	}
	/**
	 * passwordを設定します。
	 * @param password password
	 */
	public void setPassword(String password) {
	    this.password = password;
	}
	/**
	 * nameを取得します。
	 * @return name
	 */
	public String getName() {
	    return name;
	}
	/**
	 * nameを設定します。
	 * @param name name
	 */
	public void setName(String name) {
	    this.name = name;
	}
	/**
	 * isStoppedを取得します。
	 * @return isStopped
	 */
	public int getIsStopped() {
	    return isStopped;
	}
	/**
	 * isStoppedを設定します。
	 * @param isStopped isStopped
	 */
	public void setIsStopped(int isStopped) {
	    this.isStopped = isStopped;
	}
	/**
	 * branchIdを取得します。
	 * @return branchId
	 */
	public int getBranchId() {
	    return branchId;
	}
	/**
	 * branchIdを設定します。
	 * @param branchId branchId
	 */
	public void setBranchId(int branchId) {
	    this.branchId = branchId;
	}
	/**
	 * departmentIdを取得します。
	 * @return departmentId
	 */
	public int getDepartmentId() {
	    return departmentId;
	}
	/**
	 * departmentIdを設定します。
	 * @param departmentId departmentId
	 */
	public void setDepartmentId(int departmentId) {
	    this.departmentId = departmentId;
	}
	/**
	 * createdAtを取得します。
	 * @return createdAt
	 */
	public Date getCreatedAt() {
	    return createdAt;
	}
	/**
	 * createdAtを設定します。
	 * @param createdAt createdAt
	 */
	public void setCreatedAt(Date createdAt) {
	    this.createdAt = createdAt;
	}
	/**
	 * updateAtを取得します。
	 * @return updateAt
	 */
	public Date getUpdateAt() {
	    return updateAt;
	}
	/**
	 * updateAtを設定します。
	 * @param updateAt updateAt
	 */
	public void setUpdateAt(Date updateAt) {
	    this.updateAt = updateAt;
	}
}
