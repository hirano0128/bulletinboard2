package beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class Comment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String text;
	private Date createdAt;
	private Date updateAt;
	private int userId;
	private int postId;
	private int branchId;
	private int departmentId;

	public static Comment build(Map<String, String[]> parameterMap, User user) {

		Comment comment = new Comment();
		comment.text = parameterMap.get("commentText")[0];
		comment.postId = Integer.parseInt(parameterMap.get("postId")[0]);
		comment.userId = user.getId();
		comment.branchId = user.getBranchId();
		comment.departmentId = user.getDepartmentId();

		return comment;
	}

	/**
	 * idを取得します。
	 * @return id
	 */
	public int getId() {
	    return id;
	}
	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
	    this.id = id;
	}
	/**
	 * textを取得します。
	 * @return text
	 */
	public String getText() {
	    return text;
	}
	/**
	 * textを設定します。
	 * @param text text
	 */
	public void setText(String text) {
	    this.text = text;
	}
	/**
	 * createdAtを取得します。
	 * @return createdAt
	 */
	public Date getCreatedAt() {
	    return createdAt;
	}
	/**
	 * createdAtを設定します。
	 * @param createdAt createdAt
	 */
	public void setCreatedAt(Date createdAt) {
	    this.createdAt = createdAt;
	}
	/**
	 * updateAtを取得します。
	 * @return updateAt
	 */
	public Date getUpdateAt() {
	    return updateAt;
	}
	/**
	 * updateAtを設定します。
	 * @param updateAt updateAt
	 */
	public void setUpdateAt(Date updateAt) {
	    this.updateAt = updateAt;
	}
	/**
	 * userIdを取得します。
	 * @return userId
	 */
	public int getUserId() {
	    return userId;
	}
	/**
	 * userIdを設定します。
	 * @param userId userId
	 */
	public void setUserId(int userId) {
	    this.userId = userId;
	}
	/**
	 * postIdを取得します。
	 * @return postId
	 */
	public int getPostId() {
	    return postId;
	}
	/**
	 * postIdを設定します。
	 * @param postId postId
	 */
	public void setPostId(int postId) {
	    this.postId = postId;
	}
	/**
	 * branchIdを取得します。
	 * @return branchId
	 */
	public int getBranchId() {
	    return branchId;
	}
	/**
	 * branchIdを設定します。
	 * @param branchId branchId
	 */
	public void setBranchId(int branchId) {
	    this.branchId = branchId;
	}
	/**
	 * departmentIdを取得します。
	 * @return departmentId
	 */
	public int getDepartmentId() {
	    return departmentId;
	}
	/**
	 * departmentIdを設定します。
	 * @param departmentId departmentId
	 */
	public void setDepartmentId(int departmentId) {
	    this.departmentId = departmentId;
	}
}
