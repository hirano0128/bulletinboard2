package beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class Post implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String title;
	private String text;
	private String category;
	private Date createdAt;
	private Date updateAt;
	private int userId;
	private int branchId;
	private int departmentId;

	/**
	 * パラメータ取得
	 * @param parameterMap
	 * @param user
	 * @return
	 */
	public static Post build(Map<String, String[]> parameterMap, User user) {

		Post post = new Post();
		post.title = parameterMap.get("title")[0];
		post.category = parameterMap.get("category")[0];
		post.text = parameterMap.get("text")[0];
		post.userId = user.getId();
		post.branchId = user.getBranchId();
		post.departmentId = user.getDepartmentId();

		return post;
	}

	/**
	 * idを取得します。
	 * @return id
	 */
	public int getId() {
	    return id;
	}
	/**
	 * idを設定します。
	 * @param id id
	 */
	public void setId(int id) {
	    this.id = id;
	}
	/**
	 * titleを取得します。
	 * @return title
	 */
	public String getTitle() {
	    return title;
	}
	/**
	 * titleを設定します。
	 * @param title title
	 */
	public void setTitle(String title) {
	    this.title = title;
	}
	/**
	 * textを取得します。
	 * @return text
	 */
	public String getText() {
	    return text;
	}
	/**
	 * textを設定します。
	 * @param text text
	 */
	public void setText(String text) {
	    this.text = text;
	}
	/**
	 * categoryを取得します。
	 * @return category
	 */
	public String getCategory() {
	    return category;
	}
	/**
	 * categoryを設定します。
	 * @param category category
	 */
	public void setCategory(String category) {
	    this.category = category;
	}
	/**
	 * createdAtを取得します。
	 * @return createdAt
	 */
	public Date getCreatedAt() {
	    return createdAt;
	}
	/**
	 * createdAtを設定します。
	 * @param createdAt createdAt
	 */
	public void setCreatedAt(Date createdAt) {
	    this.createdAt = createdAt;
	}
	/**
	 * updateAtを取得します。
	 * @return updateAt
	 */
	public Date getUpdateAt() {
	    return updateAt;
	}
	/**
	 * updateAtを設定します。
	 * @param updateAt updateAt
	 */
	public void setUpdateAt(Date updateAt) {
	    this.updateAt = updateAt;
	}
	/**
	 * userIdを取得します。
	 * @return userId
	 */
	public int getUserId() {
	    return userId;
	}
	/**
	 * userIdを設定します。
	 * @param userId userId
	 */
	public void setUserId(int userId) {
	    this.userId = userId;
	}
	/**
	 * branchIdを取得します。
	 * @return branchId
	 */
	public int getBranchId() {
	    return branchId;
	}
	/**
	 * branchIdを設定します。
	 * @param branchId branchId
	 */
	public void setBranchId(int branchId) {
	    this.branchId = branchId;
	}
	/**
	 * departmentIdを取得します。
	 * @return departmentId
	 */
	public int getDepartmentId() {
	    return departmentId;
	}
	/**
	 * departmentIdを設定します。
	 * @param departmentId departmentId
	 */
	public void setDepartmentId(int departmentId) {
	    this.departmentId = departmentId;
	}

}
