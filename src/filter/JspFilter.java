package filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * jspフィルター
 * jspファイルを直接開くことを防ぐ

 */
@WebFilter(filterName = "jspfilter", urlPatterns = { "/*" })
public class JspFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		// URIを取得
		String uri =  ((HttpServletRequest) request).getRequestURI();

		HttpSession session = ((HttpServletRequest) request).getSession();

		Map<String, String> messages = new HashMap<String, String>();

		if ( uri.matches(".+.jsp") ) {
			messages.put("jspCheck", "指定されたURLは存在しません");

			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("./");

			return;
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}