package filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import service.UserService;

/**
 * 権限フィルター

 */
@WebFilter(filterName = "authorityfilter", urlPatterns = { "/adminUser", "/editUser", "/signup" })
public class AuthorityFilter implements Filter {

	private static final int HEAD_OFFICE = 1;		// 本社
	private static final int HUMAN_RESOURCES = 1;	// 人事部

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest) request).getSession();

		// ログインユーザーを取得
		User loginUser = (User) session.getAttribute("loginUser");

		Map<String, String> messages = new HashMap<String, String>();

		if (loginUser != null) {
			// ログインユーザー情報の取得
			User user = new UserService().getUser(loginUser.getId());

			if (user.getBranchId() == HEAD_OFFICE && user.getDepartmentId() == HUMAN_RESOURCES) {
				chain.doFilter(request, response);
			} else {
				messages.put("authorityCheck", "そのページへのアクセス権限がありません");

				session.setAttribute("errorMessages", messages);
				((HttpServletResponse) response).sendRedirect("./");

				return;
			}
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}
