package filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;

/**
 * ログインフィルター

 */
@WebFilter(filterName = "loginfilter", urlPatterns = { "/*" })
public class LoginFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		// サーブレットパスを取得
		String path = ((HttpServletRequest) request).getServletPath();

		// URIを取得
		String uri =  ((HttpServletRequest) request).getRequestURI();

		HttpSession session = ((HttpServletRequest) request).getSession();

		// ログインユーザーを取得
		User loginUser = (User) session.getAttribute("loginUser");

		Map<String, String> messages = new HashMap<String, String>();

		// 重複ログインチェック
		if ( loginUser != null && path.equals("/login") ) {
			((HttpServletResponse) response).sendRedirect("./");
			return;
		}

		// ログインチェック
		if ( loginUser == null && !(path.equals("/login")) && !(uri.matches(".+.css"))) {
			messages.put("loginCheck", "ログインしてください");

			session.setAttribute("errorMessages", messages);
			((HttpServletResponse) response).sendRedirect("login");

			return;
		} else {
			chain.doFilter(request, response);
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}
