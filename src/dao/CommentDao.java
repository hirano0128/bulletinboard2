package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import beans.Comment;
import exception.SQLRuntimeException;

public class CommentDao {
	/**
	  * コメントの追加
	  * @param connection
	  * @param comment
	  */
	public static void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments (");
			sql.append("  id");
			sql.append(", text");
			sql.append(", created_at");
			sql.append(", update_at");
			sql.append(", user_id");
			sql.append(", post_id");
			sql.append(", branch_id");
			sql.append(", department_id");
			sql.append(") VALUES (");
			sql.append("  NULL");	// id(auto_increment)
			sql.append(", ?");		// text
			sql.append(", CURRENT_TIMESTAMP");	// created_at
			sql.append(", CURRENT_TIMESTAMP");	// update_at
			sql.append(", ?");		// user_id
			sql.append(", ?");		// post_id
			sql.append(", ?");		// branch_id
			sql.append(", ?");		// department_id
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, comment.getText());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());
			ps.setInt(4, comment.getBranchId());
			ps.setInt(5, comment.getDepartmentId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * コメントのリストを取得
	  * @param connection
	  * @return
	  */
	public static List<Comment> getCommentAll(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT * FROM comments ORDER BY id");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();
			List<Comment> ret = toCommentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * SQL実行結果からコメントのリストを取得
	  * @param rs
	  * @return
	  * @throws SQLException
	  */
	private static List<Comment> toCommentList(ResultSet rs) throws SQLException {

		List<Comment> ret = new ArrayList<Comment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String text = rs.getString("text");
				Timestamp createdAt = rs.getTimestamp("created_at");
				Timestamp updateAt = rs.getTimestamp("update_at");
				int userId = rs.getInt("user_id");
				int postId = rs.getInt("post_id");
				int branchId = rs.getInt("branch_id");
				int departmentId = rs.getInt("department_id");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setText(text);
				comment.setCreatedAt(createdAt);
				comment.setUpdateAt(updateAt);
				comment.setUserId(userId);
				comment.setPostId(postId);
				comment.setBranchId(branchId);
				comment.setDepartmentId(departmentId);

				ret.add(comment);
			}

			return ret;
		} finally {
			close(rs);
		}
	}

	/**
	  * コメントの削除
	  * @param connection
	  * @param commentId
	  */
	public static void delete(Connection connection, int commentId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, commentId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	/**
	  * 指定された投稿IDに対応するコメントをすべて削除
	  * @param connection
	  * @param postId
	  */
	public static void deletePostComment(Connection connection, int postId) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE post_id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, postId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}
