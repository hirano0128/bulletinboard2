package controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.User;
import service.LoginService;

/**
 * ログイン画面

 */
@WebServlet(urlPatterns = { "/login" } )
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("/login.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// ログインパラメータの取得
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");

		// ログイン
		User user = new LoginService().login(loginId, password);

		// バリデーション
		if (isValid(request, user) == true) {
			// ログインユーザーをセッションにセット
			request.getSession().setAttribute("loginUser", user);

			response.sendRedirect("./");
		} else {
			// ログイン情報をリクエストにセット
			request.setAttribute("loginId", loginId);

			// ログイン画面へ転送
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}
	}

	private boolean isValid(HttpServletRequest request, User user) {

		Map<String, String> messages = new HashMap<String, String>();

		if (user == null) {
			// エラーメッセージをセッションにセット
			messages.put("loginCheck", "ログインに失敗しました");
			request.getSession().setAttribute("errorMessages", messages);

			return false;
		} else {
			return true;
		}
	}
}
