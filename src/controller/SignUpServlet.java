package controller;

import static utils.ValidationUtil.*;
import static validator.UserValidator.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Branch;
import beans.Department;
import beans.User;
import service.BranchService;
import service.DepartmentService;
import service.UserService;

/**
 * ユーザー新規登録画面

 */
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		// 支店と部署・役職のリストの取得
		List<Branch> branchList = new BranchService().getBranchAll();
		List<Department> departmentList = new DepartmentService().getDepartmentAll();

		request.setAttribute("branchList", branchList);
		request.setAttribute("departmentList", departmentList);

		request.getRequestDispatcher("/signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		// ユーザー情報の入力
		User user = User.build(request.getParameterMap());

		// バリデーション
		if (isValid(request, user)) {
			// 支店と部署・役職のリストの取得
			List<Branch> branchList = new BranchService().getBranchAll();
			List<Department> departmentList = new DepartmentService().getDepartmentAll();

			request.setAttribute("branchList", branchList);
			request.setAttribute("departmentList", departmentList);

			request.getRequestDispatcher("/signup.jsp").forward(request, response);
			return;
		}

		// ユーザーの登録
		new UserService().register(user);

		response.sendRedirect("adminUser");
	}

	private boolean isValid(HttpServletRequest request, User user) {

		Map<String, String> messages = new HashMap<String, String>();
		String confirmation = request.getParameter("confirmation");

		// ログインID
		messages.put("loginId", isValidLoginId(user));

		// パスワード
		messages.put("password", isValidPassword(user));

		// パスワードの再入力
		messages.put("confirmation", isValidConfirmation(user.getPassword(), confirmation));

		// 名称
		messages.put("name", isValidName(user));

		// 支店
		messages.put("branch", isValidBranch(user));

		// 部署・役職
		messages.put("department", isValidDepartment(user));

		if (isNullMapValue(messages)) {
			return false;
		} else {
			// エラーメッセージをリクエストに追加
			request.getSession().setAttribute("errorMessages", messages);
			// フォームに入力したデータを保持
			request.setAttribute("user", user);
			request.setAttribute("confirmation", confirmation);

			return true;
		}
	}

}
