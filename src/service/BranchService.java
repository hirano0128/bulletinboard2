package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Branch;
import dao.BranchDao;

public class BranchService {
	/**
	  * すべての支店を取得
	  * @return
	  */
	public List<Branch> getBranchAll() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<Branch> ret = BranchDao.getBranchAll(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 支店IDから支店を取得
	  * @param branchId
	  * @return
	  */
	public Branch getBranch(int branchId) {

		Connection connection = null;
		try {
			connection = getConnection();

			Branch ret = BranchDao.getBranch(connection, branchId);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 支店の名前から支店を取得
	  * @param branchName
	  * @return
	  */
	public Branch getBranch(String branchName) {

		Connection connection = null;
		try {
			connection = getConnection();

			Branch ret = BranchDao.getBranch(connection, branchName);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 支店の名前から支店IDを取得
	  * @param branchName
	  * @return
	  */
	public int getBranchId(String branchName) {

		Connection connection = null;
		try {
			connection = getConnection();

			int ret = BranchDao.getBranchId(connection, branchName);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 支店IDから支店の名前を取得
	  * @param branchId
	  * @return
	  */
	public String getBranchName(int branchId) {

		Connection connection = null;
		try {
			connection = getConnection();

			String ret = BranchDao.getBranchName(connection, branchId);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 支店が存在することを確かめる
	  * @param branchId
	  * @return
	  */
	public boolean isExists(int branchId) {

		Connection connection = null;
		try {
			connection = getConnection();

			if (BranchDao.isExists(connection, branchId)) {
				return true;
			} else {
				return false;
			}

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
