package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.Comment;
import dao.CommentDao;

public class CommentService {
	/**
	  * コメントの登録
	  * @param comment
	  */
	public void register(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao.insert(connection, comment);

			commit(connection);

		} catch (RuntimeException e){
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * すべてのコメントを取得
	  * @return
	  */
	public List<Comment> getCommentAll() {

		Connection connection = null;
		try {
			connection = getConnection();

			List<Comment> commentList = CommentDao.getCommentAll(connection);

			commit(connection);

			return commentList;
		} catch (RuntimeException e){
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * コメントの削除
	  * @param commentId
	  */
	public void deleteComment(int commentId) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao.delete(connection, commentId);

			commit(connection);
		} catch (RuntimeException e){
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/**
	  * 投稿のコメントを削除
	  * @param postId
	  */
	public void deletePostComment(int postId) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao.deletePostComment(connection, postId);

			commit(connection);
		} catch (RuntimeException e){
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
