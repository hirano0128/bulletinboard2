package utils;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import exception.IORuntimeException;
import exception.SQLRuntimeException;

/**
  * リソース開放ユーティリティ(Closeメソッド)

  */
public class CloseableUtil {

	/**
	  * システムリソースの開放
	  * @param closeable
	  */
	public static void close(Closeable closeable) {

		if (closeable == null) {
			return;
		}

		try {
			closeable.close();
		} catch (IOException e) {
			throw new IORuntimeException(e);
		}
	}

	/**
	  * DBへの接続解除
	  * @param connection
	  */
	public static void close(Connection connection) {

		if (connection == null) {
			return;
		}

		try {
			connection.close();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	  * Statementインターフェースの開放
	  * @param statement
	  */
	public static void close(Statement statement) {

		if (statement == null) {
			return;
		}

		try {
			statement.close();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}

	/**
	  * クエリ実行結果の開放
	  * @param rs
	  */
	public static void close(ResultSet rs) {

		if (rs == null) {
			return;
		}

		try {
			rs.close();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		}
	}
}
