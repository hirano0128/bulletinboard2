/**
 *
 */

	function postDialog() {
		if (window.confirm("この投稿を削除します\nよろしいですか？")) {
			location.href = "adminUser";
		} else {
			return false;
		}
	}

	function commentDialog() {
		if (window.confirm("このコメントを削除します\nよろしいですか？")) {
			location.href = "adminUser";
		} else {
			return false;
		}
	}

	function stopDialog() {
		if (window.confirm("ユーザーを停止します\nよろしいですか？")) {
			location.href = "adminUser";
		} else {
			return false;
		}
	}

	function activeDialog() {
		if (window.confirm("ユーザーを復活します\nよろしいですか？")) {
			location.href = "adminUser";
		} else {
			return false;
		}
	}

	function deleteDialog() {
		if (window.confirm("ユーザーを削除します\nよろしいですか？")) {
			location.href = "adminUser";
		} else {
			return false;
		}
	}

	var select = document.getElementById('branch');

	select.onchange = function selectBox1(){ // セレクトボックスに変更が加えられたら
		var indx = document.signup.branches.selectedIndex; // selectedImdexは「今」選択されているoptionを指す。返り値は数字。

		var val = document.signup.branches.options[indx].value; // optionのvalueを取得

		return val;
	}

	$(function() {
		$(".datepicker").datepicker({
		    language: 'ja',
		    format: "yyyy-mm-dd",
		    autoclose: true,
		    orientation: "bottom auto"
		});

		$("#firstDate").datepicker({
			maxDate : "0y"
		});

		$("#firstDate").change(function() {
			$("#lastDate").datepicker("option", "minDate", $("#firstDate").val());
		});

		$("#lastDate").change(function() {
			$("#firstDate").datepicker("option", "maxDate", $("#lastDate").val());
		});

		$("#lastDate").datepicker({
			maxDate : "0y"
		});
	});

	$(function() {
	    $(".commentBlock").click(function(){
	        $(".commentd").show();
	        $(".commentBlock").hide();
	        $(".commentNone").show();
	    });
	});

	$(function() {
	    $(".commentNone").click(function(){
	        $(".comment").hide();
	        $(".commentBlock").show();
	        $(".commentNone").hide();
	    });
	});
